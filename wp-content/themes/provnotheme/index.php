<?php get_header(); ?>

		<div style="padding:0px; margin-top: 5.6em">
		    <div class="container-fluid" style="width: 140%;">
				<div class="row">
					<div class="col-md-5 row1-categories" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/ski.jpg');"></div>
					<div class="col-md-2 row1-categories" style="background-color:#1ab583;"></div>
					<div class="col-md-5 row1-categories" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/jog.jpg');"></div>
				</div>
				<div class="row">
					<div class="col-md-3 row2-categories" style="background-color:#f05973;"></div>
					<div class="col-md-7 row2-categories" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/climb.jpg');"></div>
					<div class="col-md-2 row2-categories" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/hike.jpg');"></div>
				</div>
				<div class="row">
					<div class="col-md-3 row3-categories" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/run.jpg');"></div>
					<div class="col-md-3 row3-categories" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/paralympics.jpg');"></div>
					<div class="col-md-6 row3-categories" style="background-color:#00b2c9;"></div>
				</div>
		    </div>
		    <!-- /.container-fluid -->

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- wrapper -->
<?php get_footer(); ?>