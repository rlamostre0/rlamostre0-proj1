<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'g-Onv{TY.YYe/*A?febW$-iF;[xhe5]xc4t DsR*Bt9J,PW%Y]OW7Vj1rrR:<)h,');
define('SECURE_AUTH_KEY',  '?[=aI~cN:y+NnL6MXi`9X@Qi}{z!d[z8JI1==i!,b{?9y@V2uqtFHI_]:eIPgcfZ');
define('LOGGED_IN_KEY',    'ruW2GhzyyHaG9.PoAh5I3cFy[5P7?1q`GNq#*cz_7~S,$;T(*u4KPX0;@S>(aZ-u');
define('NONCE_KEY',        '$.W+o2e^|??VnAfe$bA:l:#eRTXtz5-I;{v*d$[d )9<rK#ZA4:lcaS/&qXc.Cq3');
define('AUTH_SALT',        '&l]>7q-<8#mzKZGs9AZZ-j-_SH>c|fj)/IHchn&b|N+:$5]H?P&HAZHV/6wOx+&O');
define('SECURE_AUTH_SALT', 'AtSfMm[GbPc]jW9u0wXWevn^RC!@a(/5I*#*0V1}(zP:c49@~ScOQV%+pH^+/-/v');
define('LOGGED_IN_SALT',   'nz<j&U2N@%`-<VEvAs;YH];uf?-lGx+m*qB3G#Gec4`rvSj!>;U/2C=o(ir2IWmq');
define('NONCE_SALT',       'K}WyFD1,m#mf{LJ-nU{-sY_zE83tdPEzu];Y2x@xoSy[}4I6|N@P-6|<(|wB-fc>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
