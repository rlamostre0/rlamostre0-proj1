<?php

/**
 * Template Name: News Page
 */

get_header(); ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">

		<div id="isotope-news" class="isotope">
		<?php
		//========= old code ================
			// <div id="gallery">
			// <div class="ftg-items">
				// < ? php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ? >
					// <div class="tile" ><!--style="background:url('< ? php //echo $image[0]; ? >') no-repeat;"-->
					// test
						// <a class="tile-inner" href="< ? php echo $image[0]; ? >">
								// <img class="item" src="< ? php echo $image[0]; ? >" />< ? php //the_title(); ? >
							// </a>
					// </div>
					// $('#gallery').finalTilesGallery();

			// </div>
			// </div>
		//========= end old code ================
		?>
		
		
			<?php 
			query_posts('post_type=news'); 
			while ( have_posts() ) : the_post(); 
			?>		
				<?php
				$categoryName=get_the_terms( $id, 'news categories' );
				$tempRow=null;
				if($categoryName){
					foreach($categoryName as $key=>$val){
						$tempRow=$key;
					}
					$categoryName=$categoryName[$tempRow]->name;
				}
				$allowedSize=array(0,1,2);
				$width=rand(0,2);
				$width=($allowedSize[$width]!=0?'width'.$allowedSize[$width]:'');
				$allowedSize=array(0,1,2);
				$height=rand(0,2);
				$height=($allowedSize[$height]!=0?'height'.$allowedSize[$height]:'');
				$featuredImage=wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$featuredImage=$featuredImage[0];
				?>
					<div class="element-item <?php echo ($categoryName!=false?'color'.$categoryName:''); ?> <?php echo $height.' '.$width; ?>" 
					style="<?php echo (isset($featuredImage)?"background-image: url('".$featuredImage."');background-size:cover;":""); ?>">
						<p class="symbol <?php echo ($categoryName=='Trening'?'textBlack':'textWhite'); ?>" style="font-size:1em"><?php echo the_title(); ?></p>
					</div>
			<?php endwhile; // end of the loop. ?>
		</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
<style>
* {
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
}
/* clear fix */
.isotope:after {
  content: '';
  display: block;
  clear: both;
}

/* ---- .element-item ---- */

.element-item {
  position: relative;
  float: left;
  width: 25%;
  height: 200px;
  margin: 5px;
  padding: 10px;
  background: #888;
  color: #262524;
}

.element-item.width2 { width: 25%; }
.element-item.height2 { height: 410px; }

.element-item > * {
  margin: 0;
  padding: 0;
}

.element-item .symbol {
  position: absolute;
  left: 10px;
  top: 0px;
  font-size: 42px;
  font-weight: bold;
}

.rainbowed > *:nth-child(10n+0) { background: hsl(   0, 100%, 50%); }
.rainbowed > *:nth-child(10n+1) { background: hsl(  36, 100%, 50%); }
.rainbowed > *:nth-child(10n+2) { background: hsl(  72, 100%, 50%); }
.rainbowed > *:nth-child(10n+3) { background: hsl( 108, 100%, 50%); }
.rainbowed > *:nth-child(10n+4) { background: hsl( 144, 100%, 50%); }
.rainbowed > *:nth-child(10n+5) { background: hsl( 180, 100%, 50%); }
.rainbowed > *:nth-child(10n+6) { background: hsl( 216, 100%, 50%); }
.rainbowed > *:nth-child(10n+7) { background: hsl( 252, 100%, 50%); }
.rainbowed > *:nth-child(10n+8) { background: hsl( 288, 100%, 50%); }
.rainbowed > *:nth-child(10n+9) { background: hsl( 324, 100%, 50%); }

.colorSport{background-color:#00bae4;}
.colorFriluftsliv{background-color:#f05973;} 
.colorTrening{background-color:#ffffff;} 
.colorKosthold {background-color:#1ab583;}
.colorVelvare {background-color:#fdbb4e;}
.textBlack{color:#000000;}
.textWhite{color:#ffffff}
</style>
<script>
$( function() {
  // init Isotope
  var $container = $('#isotope-demo').isotope({
    itemSelector: '.element-item',
    layoutMode: 'masonry',
    masonry: {
      columnWidth: 210
    },
    cellsByRow: {
      columnWidth: 420,
      rowHeight: 420
    },
    masonryHorizontal: {
      rowHeight:410
    },
    cellsByColumn: {
      columnWidth: 420,
      rowHeight: 420
    }
  });
});
</script>