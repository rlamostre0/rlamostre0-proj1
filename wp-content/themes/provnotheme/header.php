<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php wp_title(' | ', true, 'right'); ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php bloginfo("template_directory"); ?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php bloginfo("template_directory"); ?>/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php bloginfo("template_directory"); ?>/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php bloginfo("template_directory"); ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php bloginfo("template_directory"); ?>/css/main.css" rel="stylesheet">

   <style type="text/css">
	@font-face {
	    font-family: GothamRoundedBook;
	    src: url('<?php bloginfo("template_directory"); ?>/fonts/GothamRnd-Book.otf');
	}

	@font-face {
	    font-family: GothamRoundedBold;
	    src: url('<?php bloginfo("template_directory"); ?>/fonts/GothamRnd-Bold.otf');
	}

	body {
		font-family: GothamRoundedBold;
		
		color: #24292C;
	}
	
	a {
		color:#464746;
		text-decoration: none;
	}

	a:hover {
		color: black;
		text-decoration: none;
	}
   </style>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top col-md-12" role="navigation" style="background-color:#eef0ef; min-height: 80px">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header col-md-2" style="margin-top: 0px; padding: 0em">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h2><a class="menu-color site-title" href="index.html">prøv.no</a></h2>
            </div>

			<div class="center-menu col-md-8 text-center" style="margin-top: 0.4em;">
				<ul class="list-inline">
					<li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/button-links/1.png"></a></li>
					<li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/button-links/2.png"></a></li>
					<li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/button-links/3.png"></a></li>
					<li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/button-links/4.png"></a></li>
				</ul>
			</div 
						
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                	<div class="row" style="margin-top: 2em; margin-right: 20px">
	                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Stine Pjaaten</a>
    	                <a href="#"><?php echo str_repeat('&nbsp;', 3); ?><img src="<?php bloginfo('template_directory'); ?>/images/sample-user.png" style="width: 20%"></a>
                	</div>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
			<?php get_sidebar(); ?>
		</nav>
