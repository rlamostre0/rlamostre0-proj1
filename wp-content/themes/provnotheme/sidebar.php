<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
<div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav side-nav">
        <!--li class="active"-->
			<span style="color:#FFFFFF;">Kategorier</span>
			<?php 

			$args = array(
				'type'                     => 'post',
				'orderby'                  => 'id',
				'order'                    => 'ASC',
				'hide_empty'               => 0,
				'taxonomy'                 => 'news categories',
				'pad_counts'               => false 
			); 
			$categories = get_categories( $args ); 
			foreach($categories as $category){ ?> 
        <li>
            <a href="#"><?php echo $category->name; ?></a>
        </li>
			<?php 
			}
			?> 
    </ul>
</div>